# Squiggles
Generative squiggles

![squiggles](squiggles.jpg)
![1](imgs/s1.jpg)
![2](imgs/s2.jpg)
![3](imgs/s3.jpg)

Outlines are drawn in a semi-generative fashion, and are painted with a recursive flood fill algorithm.

| Outline | Flood filled|
| --- | --- |
| ![outline](outline.jpg) | ![floodfilled](floodfill.jpg) |
| ![o1](imgs/o1.jpg) | ![f1](imgs/f1.jpg) |